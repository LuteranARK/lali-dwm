/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>
#include "themes/lali.h"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 14;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
// static const char *fonts[]          = { "Iosevka Term:Semibold:size=9:antialias=true:autohint=true:lcdfilter=1" };
static const char *fonts[]          = { "PragmataProMonoLiga Nerd Font:Bold:size=8.5:antialias=true:autohint=true:lcdfilter=1",
                                        "Source Han Sans KR:Bold:size=8.5"};
static const char dmenufont[]       = "PragmataProMonoLiga Nerd Font:Bold:size=8.5:antialias=true:autohint=true:lcdfilter=1";
static const char *colors[][3]      = {
    /*               fg             bg              border   */
    [SchemeNorm] =  {lightyellow,   black,          black},
    [SchemeSel]  =  {gold,          darkpink,       dracula},
};

typedef struct {
    const char *name;
    const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spdrd", "-g", "195x47", "-e", "tmux", "-u", 0 };
// const char *spcmd1[] = {"alacritty", "--class", "spdrd", "-e", "tmux", "-u", 0 };
const char *spcmd2[] = {"st", "-n", "splf", "-g", "195x40", "-e", "lf", 0 };
// const char *spcmd2[] = {"alacritty", "msg", "create-window", "--class", "splf", "-e", "lf", 0 };
static Sp scratchpads[] = {
    /* name          cmd  */
    {"spdrd",      spcmd1},
    {"splf",       spcmd2},
};

/* tagging */
static const char *tags[] = { "[1]:net", "[2]:média", "[3]:zene", "[4]:term", "[5]:munka", "[6]:fájl", "[7]:kép", "[8]:mail", "[9]:egyéb" };
static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class                instance    title       tags mask   switchtotag isfloating   monitor */
    { "firefox",            0,		    0,		    1 << 0,			0,		0,			 -1 },
    { "firefox",            "Browser",	0,		    1 << 0,			0,		1,			 -1 },
    { "firefox",            "Toolkit", "Kép a képben", 1 << 0,      0,      1,			 -1 },
    { "firefox",            "Places",   "Könyvtár", 1 << 0,         0,      1,			 -1 },
    { "Chromium",           0,		    0,		    1 << 0,			0,		0,			 -1 },
    { "qutebrowser",        0,		    0,		    1 << 0,			0,		0,			 -1 },
    { "Brave-browser",      0,	        0,		    1 << 0,	 		0,	    0,			 -1 },
    { "firefox-nightly",    0,		    0,          1 << 0,			0,		0,			 -1 },
    { "firefox-nightly",    "Browser",	0,          1 << 0,			0,		1,			 -1 },
    { "firefox-nightly", "Toolkit", "Picture-in-Picture", 1 << 0,   0,      1,			 -1 },
    { "firefox-nightly",    "Places",   "Library",  1 << 0,         0,      1,			 -1 },
    { "mpv",  	            0,		    0,		    1 << 1,			1,		0,			 -1 },
    { "chatterino",         0,		    0,		    1 << 1,			1,		0,			 -1 },
    { 0, 	                "muzsika",	0,		    1 << 2,			0,		0,			 -1 },
    { 0, 	                "mus2",		0,		    1 << 2,			1,		0,			 -1 },
    { "cantata",            0,		    0,	        1 << 2,			0,		0,			 -1 },
    { 0,                    "mail",		0,		    1 << 7,			0,		0,			 -1 },
    { 0,                    "asmr",		"ASMR",		1 << 8,			0,		0,			 -1 },
    { "Pcmanfm",            0,		    0,		    0 << 0,			0,		1,			 -1 },
    { 0,		            "spdrd",	0,		    SPTAG(0),		0,	    1,			 -1 },
    { 0,		            "splf",		0,		    SPTAG(1),		0,	    1,			 -1 },
    { "Gvim",               0,		    0,		    0 << 0,			0,		1,			 -1 },
    { "Seahorse",           0,		    0,		    0 << 0,			0,		1,			 -1 },
    { "Pulseaudio-equalizer-gtk", 0,	0,          0 << 0,	        0,	    1,	         -1 },
    { "Mousepad",           0,		    0,	    	0 << 0,			0,		1,			 -1 },
    { "Engrampa",           0,		    0,  		0 << 0,			0,		1,			 -1 },
    { "Dragon-drop",        0,	        0,	    	0 << 0,			0,		1,			 -1 },
    { "Lxappearance",       0,	        0,	    	0 << 0,			0,		1,			 -1 },
    { "Nsxiv",              0,		    0,		    1 << 6,			1,		0,			 -1 },
    { "imv",                0,		    0,		    1 << 6,			1,		0,			 -1 },
    { "Gcr-prompter",       0,	        0,		    0 << 0,			0,		1,			 -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },    /* first entry is default */
    { "><>",      0 },    /* no layout function means floating behavior */
    { "[M]",      monocle },
    { "|||",      col },
    { "[D]",      deck },
};

/* key definitions */
#define MODKEY Mod4Mask
#define MOD1KEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, 0 } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run2", "-m", dmenumon, "-l", "8", "-fn", dmenufont, "-sb", darkpink, 0 };
static const char *dmenu2[]  = { "j4-dmenu-desktop", "--dmenu=dmenu -i -c -l 8 -bw 3 -p run:", "--term=xterm", 0 };
static const char *termcmd[]  = { "st", 0 };
static const char *termcmd2[]  = { "xterm", 0 };
// static const char *termcmd2[]  = { "alacritty", "msg", "create-window", 0 };
static const char *quteb[]  = { "qutebrowser", 0 };
static const char *brave[]  = { "brave", "--ignore-gpu-blocklist", "--enable-accelerated-video-decode", "--enable-features=Vulkan,VaapiVideoDecoder", 0 };
// static const char *fire[]  = { "/opt/firefox-nightly/firefox", 0 };
static const char *fire[]  = { "firefox", 0 };
static const char *chrom[]  = { "chromium", 0 };
static const char *lock[]  = { "lock.sh", 0 };
static const char *clipmpv[]  = { "clipmpv", 0 };
static const char *fmgui[]  = { "pcmanfm", 0 };
static const char *vidp[]  = { "vidplay", 0 };
static const char *asmr[]  = { "asmrplay", 0 };
static const char *asmr2[]  = { "asmrplay2", 0 };
static const char *twplay[]  = { "twitch-play.sh", 0 };
static const char *twnot[]  = { "twitch-notify.sh", 0 };
static const char *clip[]  = { "clipmenu", "-p", "Másol:", 0 };
static const char *pass[]  = { "passmenu", "-l", "10", "-p", "Jelszó:", 0 };
static const char *dkill[]  = { "dmenukill", 0 };
static const char *ttog[]  = { "torrenttoggle", 0 };
// static const char *ptog[]  = { "psdtoggle" };
static const char *rtog[]  = { "redtoggle.sh", 0 };
static const char *ctog[]  = { "cputoggle", 0 };
static const char *ranw[]  = { "randwall.sh" };
static const char *cstat[]  = { "cpustat.sh", 0 };
static const char *bat[]  = { "batupd.sh", 0 };
static const char *nottor[]  = { "keyboardtoggle", 0 };
static const char *pictog[]  = { "picomtoggle", 0 };
static const char *demoji[]  = { "dmenuunicode", 0 };
static const char *ford[]  = { "fordit", 0 };
static const char *mount[]  = { "dmenumount", 0 };
static const char *umount[]  = { "dmenuumount", 0 };
static const char *dyt[]  = { "ytfzf", "-t", "--thumb-viewer=chafa", "--async-thumbnails", "--detach", "-D", 0 };
static const char *exitc[]  = { "exitwm", 0 };
static const char *pri[]  = { "printscreen", 0 };
static const char *prifull[]  = { "fullscreenshot", 0 };
// static const char *slock[]  = { "slock", 0 };
static const char *eso[]  = { "esoradar", 0 };
static const char *mpcprev[]  = { "mpc", "prev", 0 };
static const char *mpcnext[]  = { "mpc", "next", 0 };
// static const char *mpcpause[]  = { "mpc", "pause", 0 };
static const char *mpctoggle[]  = { "mpc", "toggle", 0 };
// static const char *mpcstop[]  = { "mpc", "stop", 0 };
static const char *mpcback[]  = { "mpc", "seekthrough", "-00:00:5", 0 };
static const char *mpcforw[]  = { "mpc", "seekthrough", "+00:00:5", 0 };
static const char *mus[]  = { "xterm", "-name", "muzsika", "-e", "ncmpcpp", 0 };
// static const char *mus[]  = { "alacritty", "msg", "create-window", "--class", "muzsika", "-e", "ncmpcpp", 0 };
static const char *mail[]  = { "xterm", "-name", "mail", "-e", "neomutt", 0 };
// static const char *mail[]  = { "alacritty", "msg", "create-window", "--class", "mail", "-e", "neomutt", 0 };
static const char *calc[]  = { "rofi", "-show", "calc", "-modi", "calc", "-no-show-match", "-no-sort", "-no-history", 0 };
static const char *dunsts[]  = { "dunstctl", "history-pop", 0 };
static const char *dunstc[]  = { "dunstctl", "close-all", 0 };
static const char *claws[]  = { "claws-mail", 0 };
static const char *search[]  = { "srdmenu", 0 };
static const char *ncplay[]  = { "xterm", "-name", "mus2", "-e", "tncplay.sh", 0 };
// static const char *ncplay[]  = { "alacritty", "msg", "create-window", "--class", "mus2", "-e", "tncplay.sh", 0 };
// static const char *dhtop[] = { "alacritty", "msg", "create-window", "-e", "htop", 0 };
static const char *dhtop[] = { "xterm", "-e", "htop", 0 };
// static const char *dpmixer[] = { "alacritty", "msg", "create-window", "-e", "pulsemixer", 0 };
static const char *dpmixer[] = { "xterm", "-e", "pulsemixer", 0 };

static const Key keys[] = {
    /* modifier                     key        function        argument */
    { MOD1KEY|MODKEY,               XK_space,  spawn,          {.v = dmenucmd } },
    { MODKEY,                       XK_space,  spawn,          {.v = dmenu2} },
    { MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd2 } },
    { MODKEY,                       XK_t,      spawn,          {.v = twplay } },
    { MODKEY,                       XK_p,      spawn,          {.v = vidp } },
    { MOD1KEY|MODKEY,               XK_a,      spawn,          {.v = asmr } },
    { MODKEY|ShiftMask,             XK_aacute, spawn,          {.v = asmr2 } },
    { MODKEY|ShiftMask,             XK_f,      spawn,          {.v = fire } },
    { MODKEY|ShiftMask,             XK_c,      spawn,          {.v = chrom } },
    { MODKEY|ShiftMask,             XK_b,      spawn,          {.v = brave } },
    { MODKEY|ShiftMask,             XK_v,      spawn,          {.v = quteb } },
    { MODKEY|ShiftMask,             XK_x,      spawn,          {.v = lock } },
    // { MOD1KEY|MODKEY,            XK_o,      spawn,          {.v = twnot } },
    { MODKEY,                       XK_o,      spawn,          {.v = twnot } },
    { MOD1KEY|MODKEY,               XK_h,      spawn,          {.v = clip } },
    { MOD1KEY|MODKEY,               XK_p,      spawn,          {.v = pass } },
    { MOD1KEY|MODKEY,               XK_aacute, spawn,          {.v = dkill } },
    { MODKEY|ControlMask,           XK_t,      spawn,          {.v = ttog } },
    // { MODKEY|ControlMask,        XK_aacute, spawn,          {.v = ptog } },
    { MODKEY|ControlMask,           XK_udoubleacute, spawn,    {.v = rtog } },
    { MODKEY|ControlMask,           XK_odoubleacute, spawn,    {.v = ranw} },
    { MODKEY|ControlMask,           XK_uacute, spawn,          {.v = ctog } },
    { MODKEY|ControlMask,           XK_udiaeresis, spawn,      {.v = cstat } },
    { MODKEY|ControlMask,           XK_oacute, spawn,          {.v = bat } },
    { MODKEY|ControlMask,           XK_odiaeresis, spawn,      {.v = nottor } },
    { MODKEY|ControlMask,           XK_eacute, spawn,          {.v = pictog } },
    { MOD1KEY|MODKEY,               XK_e,      spawn,          {.v = demoji } },
    { MOD1KEY|MODKEY,               XK_l,      spawn,          SHCMD("dmenuhandler $(xsel -b)") },
    { MODKEY,                       XK_F12,    spawn,          {.v = pri } },
    { 0,                            XK_F12,    spawn,          {.v = prifull } },
    // { MODKEY|ShiftMask,          XK_p,      spawn,          SHCMD("mpv $(xsel -b)") },
    { MODKEY|ShiftMask,             XK_p,      spawn,          {.v = clipmpv } },
    { MODKEY|ShiftMask,             XK_t,      spawn,          {.v = fmgui } },
    { MOD1KEY|MODKEY,               XK_f,      spawn,          {.v = ford } },
    { MOD1KEY|MODKEY,           	XK_m,      spawn,          {.v = mount } },
    { MOD1KEY|MODKEY,           	XK_u,      spawn,          {.v = umount } },
    { MOD1KEY|MODKEY,           	XK_odoubleacute, spawn,    {.v = eso } },
    { MODKEY,                       XK_Escape, spawn,          {.v = exitc } },
    { MOD1KEY|MODKEY,               XK_c,      spawn,          {.v = calc } },
    { MODKEY|ShiftMask,             XK_n,      spawn,          {.v = mus } },
    { MODKEY|ControlMask,           XK_n,      spawn,          {.v = ncplay } },
    { MODKEY|ShiftMask,             XK_m,      spawn,          {.v = mail } },
    { MODKEY|ControlMask,           XK_comma,  spawn,          {.v = mpcback } },
    { MODKEY|ControlMask,           XK_period, spawn,          {.v = mpcforw } },
    { MOD1KEY|ControlMask,          XK_p,      spawn,          SHCMD("mpvc -p") },
    { MOD1KEY|ControlMask,          XK_comma,  spawn,          SHCMD("mpvc -t -10") },
    { MOD1KEY|ControlMask,          XK_period, spawn,          SHCMD("mpvc -t 10") },
    { MOD1KEY|ControlMask,          XK_eacute, spawn,          SHCMD("mpvc -v -2") },
    { MOD1KEY|ControlMask,          XK_aacute, spawn,          SHCMD("mpvc -v 2") },
    { MOD1KEY|ControlMask,          XK_k,      spawn,          SHCMD("mpvc -k") },
    { MOD1KEY|MODKEY,               XK_d,      spawn,          {.v = dyt } },
    { MODKEY,                       XK_n,      togglebar,      {0} },
    { MODKEY,                       XK_comma,  focusmaster,    {0} },
    { MODKEY,                       XK_period, focusmaster,    {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    { MODKEY,                       XK_Right,  focusstack,     {.i = +1 } },
    { MODKEY,                       XK_Down,   focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,                       XK_Left,   focusstack,     {.i = -1 } },
    { MODKEY,                       XK_Up,     focusstack,     {.i = -1 } },
    { MODKEY|ShiftMask,             XK_j,      incnmaster,     {.i = +1 } },
    { MODKEY|ShiftMask,             XK_k,      incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,      setmfact,       {.f = -0.01} },
    { MODKEY,                       XK_l,      setmfact,       {.f = +0.01} },
    { MOD1KEY|MODKEY,               XK_k,      setcfact,       {.f = +0.25} },
    { MOD1KEY|MODKEY,               XK_j,      setcfact,       {.f = -0.25} },
    { MOD1KEY|MODKEY,               XK_n,      setcfact,       {.f =  0.00} },
    { MODKEY,                       XK_0,      view,           {0} },
    { MODKEY,                       XK_q,      killclient,     {0} },
    { MODKEY,                       XK_w,      setlayout,      {.v = &layouts[0]} },
    //{ MODKEY|ShiftMask,           XK_f,      setlayout,      {.v = &layouts[1]} },
    { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
    { MODKEY,                       XK_c,      setlayout,      {.v = &layouts[3]} },
    { MODKEY,                       XK_r,      setlayout,      {.v = &layouts[4]} },
    { MODKEY,                       XK_f,      togglefullscr,  {0} },
    // { MODKEY,                    XK_o,      setlayout,      {0} },
    { MODKEY|ControlMask,           XK_space,  togglefloating, {0} },
    //{ MODKEY,                     XK_b,      view,           {.ui = ~0 } },
    //{ MODKEY,             		XK_v,      tag,            {.ui = ~0 } },
    //{ MODKEY,                     XK_comma,  focusmon,       {.i = -1 } },
    //{ MODKEY,                     XK_period, focusmon,       {.i = +1 } },
    //{ MODKEY|ShiftMask,           XK_comma,  tagmon,         {.i = -1 } },
    //{ MODKEY|ShiftMask,           XK_period, tagmon,         {.i = +1 } },
    { MODKEY,		                XK_Tab,    shiftviewclients, { .i = +1 } },
    { MODKEY|ShiftMask,             XK_Tab,    shiftviewclients, { .i = -1 } },
    { MODKEY|ControlMask,           XK_h, 	   zoom,           {0} },
    { MODKEY|ControlMask,           XK_l, 	   zoom,           {0} },
    { MODKEY|ControlMask,           XK_Left,   zoom,           {0} },
    { MODKEY|ControlMask,           XK_Right,  zoom,           {0} },
    { MODKEY|ControlMask,			XK_Down,   pushdown,       {0} },
    { MODKEY|ControlMask,       	XK_Up,     pushup,         {0} },
    { MODKEY|ControlMask,			XK_j,      pushdown,       {0} },
    { MODKEY|ControlMask,       	XK_k,      pushup,         {0} },
    { MODKEY,            			XK_a,  	   togglescratch,  {.ui = 0 } },
    { MODKEY,            			XK_s,	   togglescratch,  {.ui = 1 } },
    { ControlMask,            		XK_0,	   spawn,          {.v = dunsts } },
    { ControlMask,            		XK_space,  spawn,          {.v = dunstc } },
    { MOD1KEY|ControlMask, 	        XK_m,	   spawn,		   SHCMD("miniwin.sh; sigdsblocks 11") },
    { 0, 		XF86XK_MonBrightnessUp,		   spawn,		   SHCMD("light -A 20; notify-send -t 750 $(light -G)%") },
    { 0, 		XF86XK_MonBrightnessDown,	   spawn,		   SHCMD("light -U 20; notify-send -t 750 $(light -G)%") },
    // { 0, 		XF86XK_AudioMute,		   spawn,		   SHCMD("amixer -D pulse sset Master toggle; kill -44 $(pidof dwmblocks)") },
    { 0, 		XF86XK_AudioMute,			   spawn,		   SHCMD("pactl set-sink-mute alsa_output.pci-0000_00_1f.3.analog-stereo toggle; sigdsblocks 10") },
    { 0, 		                    XK_F9,     spawn,		   SHCMD("pactl set-sink-mute alsa_output.pci-0000_00_1f.3.analog-stereo toggle; sigdsblocks 10") },
    { 0, 		XF86XK_AudioRaiseVolume,	   spawn,		   SHCMD("pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo +5%; sigdsblocks 10") },
    // { 0, 		                    XK_F11,	   spawn,		   SHCMD("pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo +5%; kill -44 $(pidof dsblocks)") },
    { 0, 		XF86XK_AudioLowerVolume,	   spawn,		   SHCMD("pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo -5%; sigdsblocks 10") },
    // { 0, 		                    XK_F10,	   spawn,		   SHCMD("pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo -5%; kill -44 $(pidof dsblocks)") },
    // { MODKEY, 	XF86XK_Sleep,	   		   spawn,		   {.v = slock } },
    // { MODKEY|ControlMask, 	XF86XK_Sleep,  spawn,		   {.v = susp } },
    { 0,        XF86XK_AudioPrev,		       spawn,   	   {.v = mpcprev } },
    { 0,                            XK_F6,     spawn,   	   {.v = mpcprev } },
    { MODKEY|ControlMask,           XK_v,      spawn,   	   {.v = mpcprev } },
    { 0,        XF86XK_AudioNext,		       spawn,		   {.v = mpcnext } },
    { 0,                            XK_F8,	   spawn,		   {.v = mpcnext } },
    { MODKEY|ControlMask,           XK_b,      spawn,   	   {.v = mpcnext } },
    // { 0,        XF86XK_AudioPause,          spawn,		   {.v = mpcpause } },
    { 0,        XF86XK_AudioPlay,		       spawn,		   {.v = mpctoggle } },
    { 0,                            XK_F7,	   spawn,		   {.v = mpctoggle } },
    { MODKEY|ControlMask,           XK_p,      spawn,   	   {.v = mpctoggle } },
    // { 0,        XF86XK_AudioStop,		   spawn,		   {.v = mpcstop } },
    { 0,        XF86XK_Mail,		           spawn,		   {.v = claws } },
    // { 0,        XF86XK_HomePage,		       spawn,		   {.v = home } },
    { 0,        XF86XK_Search,		           spawn,		   {.v = search } },
    { 0,        XF86XK_Calculator,		       spawn,		   {.v = calc } },
    { 0,        XF86XK_Tools,		           spawn,		   {.v = ncplay } },
    TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_9,                      8)
    { MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} },
    { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {.v = &layouts[0]} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkLtSymbol,          0,              Button2,        setlayout,      {.v = &layouts[3]} },
    { ClkWinTitle,          0,              Button2,        spawn,          SHCMD("miniwin.sh; sigdsblocks 11") },
    { ClkWinTitle,          0,              Button3,        togglefullscr,  {0} },
    { ClkWinTitle,          MODKEY,         Button3,        togglefloating, {0} },
    { ClkStatusText,        0,              Button1,        spawn,          SHCMD("sigdsblocks 2; sigdsblocks 3; sigdsblocks 4; sigdsblocks 5; sigdsblocks 10") },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    // { ClkStatusText,        0,              Button3,        spawn,          SHCMD("st -e htop") },
    { ClkStatusText,        0,              Button3,        spawn,          {.v = dhtop } },
    // { ClkStatusText,        MODKEY,         Button3,        spawn,          SHCMD("st -e pulsemixer") },
    { ClkStatusText,        MODKEY,         Button3,        spawn,          {.v = dpmixer } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkClientWin,         MODKEY,         Button4,        shiftviewclients, { .i = -1 } },
    { ClkClientWin,         MODKEY,         Button5,        shiftviewclients, { .i = +1 } },
    { ClkTagBar,            0,              Button4,        shiftviewclients, { .i = -1 } },
    { ClkTagBar,            0,              Button5,        shiftviewclients, { .i = +1 } },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
